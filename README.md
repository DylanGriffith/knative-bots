# ARCHIVED: This project is not maintained anymore as functionality is replicated elsewhere

# Knative Bots

This project is a collection of bots that are running on Knative. This serves
as a set of examples but also for real world Dogfooding of GitLab Serverless.

# Running Locally

You can run a bot locally like:

```
GITLAB_BASE_URL=https://gitlab.com API_TOKEN=<gitlab-api-token> WEBHOOK_SECRET=secret FUNCTION_NAME=ThroughputLabels FUNCTION_PATH=throughput_labels.rb \
  bundle exec rackup
```

Then you can test out requests to it like so:

```
curl -i -X POST \
  -H 'Content-Type: application/json' \
  -d '{"labels":[{"title":"Configure"}],"changes":{"created_by_id":123},"project":{"id":"DylanGriffith%2Fknative-bots"},"object_attributes":{"iid":1,"action":"open"}}' \
  http://user:secret@localhost:9292/
```

If that worked the output should be:

```
HTTP/1.1 200 OK
Transfer-Encoding: chunked
Server: WEBrick/1.3.1 (Ruby/2.4.4/2018-03-28)
Date: Tue, 22 Jan 2019 23:18:40 GMT
Connection: Keep-Alive

success
```

And it should have added a discussion item to https://gitlab.com/DylanGriffith/knative-bots/merge_requests/1

# Deployment

In order to deploy this project you need to do the following:

1. Add a Kubernetes cluster to the project
1. Install Helm Tiller and then Knative on the cluster
1. Set a CI variable `API_TOKEN` which will be used as an API token for GitLab
1. Set a CI variable `WEBHOOK_TOKEN` which will be used by the bot to
   authenticate incoming webhooks to ensure they are from GitLab which prevents
   spoofing the bots with webhooks from elsewhere.
1. Allow network traffic to GitLab.com from Knative (since [Knative disables
   egress traffic by
   default](https://github.com/knative/docs/blob/master/serving/outbound-network-access.md#configuring-outbound-network-access)):
   ```
   cat <<EOF | kubectl apply -f -
   apiVersion: networking.istio.io/v1alpha3
   kind: ServiceEntry
   metadata:
     name: gitlab-ext
   spec:
     hosts:
     - gitlab.com
     ports:
     - number: 443
       name: https
       protocol: HTTPS
     resolution: DNS
     location: MESH_EXTERNAL
   EOF
   ```
1. Run a CI pipeline
1. Find the domain name for your deployed bot on `Operations > Serverless`
1. Configure webhooks in the project you want the bot to post to. For the
   [`throughput_labels` bot](lib/throughput_labels.rb) you can create a webhook
   under `Settings > Integrations`. Set the URL to be
   `http://webhook:<$WEBHOOK_TOKEN>@<bot-domain-name>`. You also need to untick
   `Push events` and tick `Merge request events` so that the right type of
   events are sent for this bot.
